 # Viu Launchpad
Launchpad is our proven frontend build toolkit and asset pipeline to get projects off the ground quickly. It uses Gulp, Webpack and an assortment of handy tasks to develop, optimize and deploy frontends or entire static sites.

## Getting Started

### Prerequisites

Launchpad requires [NodeJS and NPM](https://nodejs.org) 18.16.0 or higher. We recommend using Node 22 and [NVM](https://github.com/creationix/nvm) to manage node versions.

### Installing

Install VIU Launchpad into your project

```
npm install @viu/launchpad --save-dev
```
### Update from v4 to v5

1. Update your devDependency to Launchpad "^5.0.0".
2. Rename `gulpfile.js` to `gulpfile.mjs` and `gulpfile.js/index.js` to `gulpfile.mjs/index.mjs` in your package.json
3. Update the Node engine in your project's `package.json` to 22.3.0 or higher and NPM to 10.0.0 or higher. If you have build scripts or an `.nvmrc` file then update them too.
4. Remove any `node_modules` folder or NPM LOCK files (`package-lock.json`, `yarn.lock`) and then run a complete `npm install` or `yarn install`.
5. Newer stylelint versions do no longer support the order/order and the order/properties-alphabetical-order rules. Remove them from your stylelint.config.js file, if you use them.
6. Since we're running on dart-sass now, building your project might throw deprecation warnings. You can silence them by adding the following to your package.json file:
```json
  "config": {
    "css": {
      "sass": {
        "silenceDeprecations": ["mixed-decls", "legacy-js-api", "import", "global-builtin"]
      }
    }
  }
```

### Update from v3 to v4

This is a major breaking update in many regards. Many dependencies have gotten major updates or they have been succeeded by more modern alternatives. Launchpad now uses [Webpack](https://webpack.js.org/) 5, [`Dart Sass`](https://sass-lang.com/dart-sass) instead of [`Node Sass`](https://github.com/sass/node-sass), [`Stylelint`](https://stylelint.io/) instead of [`Sasslint`](https://github.com/sasstools/sass-lint), [`eslint`](https://eslint.org/) instead of [`tslint`](https://palantir.github.io/tslint/). All these changes enable Launchpad to run on Node 14+ with 16.14.* being the recommended version.

Existing projects should continue to work with a few configuration changes and linting. However support for icon font generation has been removed. If you use icon fonts, consider managing them in another tool and simply storing them in `src/assets/static`.

1. Update the Node engine in your project's `package.json` to 16.14.2 or higher and NPM to 8.5.0 or higher. If you have build scripts or an `.nvmrc` file then update them too.
2. Update your environment to [NodeJS and NPM](https://nodejs.org) 16.14.2 or higher.
3. Update your devDependency to Launchpad "^4.0.0".
4. Remove any `node_modules` folder or NPM LOCK files (`package-lock.json`, `yarn.lock`) and then run a complete `npm install` or `yarn install`.
5. Handlebars and Nunjucks are now equal HTML renderers in Launchpad. There is no more html task to be configured in your project. Instead, add a `"nunjucks"` or `"handlebars"` task to the config in your `package.json` to continue rendering html. You can set the src files to be named *.html or rename your existing templates to `.njk` (Nunjucks) or `.hbs` (Handlebars). Both renderers can run in the same project but target files will be overwritten if both tasks export to `.html`.
6. As `eslint` replaced `tslint`, the `eslintrc.js` must be updated if Typescript is used in the project.
```js
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'import',
  ],
```
7. Run `npm start` and your project should be running.
8. Run `npm lint` to check if the updated linters and linting rules trigger new warnings or errors.
## Using

### File Structure
Create the following folder structure or use the example configuration from the `./demo/` folder.

* `src/assets`
  * `fonts/` (files are copied to the root folder of the output destination)
  * `images/` (files will be optimized)
  * `sprites/` (files are merged into an SVG sprite)
  * `static/` (files are copied to the root folder of the output destination)
  * `webapp/` (files are copied to the root folder of the output destination and updated with revisioned URLs)
* `src/components`
  * {component name}
    * {component name}.js
    * {component name}.scss
    * {component name}.html / .hbs
    * {component name}.json
* `src/data`
  * `global.json` (file that holds all global data)
  * [...] here you add a {template name}.json for every template that needs specific data
* `src/design` (contains the single source of truth regarding the design)
  * `breakpoints.json`
  * `colors.json`
  * `sizes.json`
  * `typography.json`
* `src/javascripts`
  * `global.js` (if you follow the example configuration below)
* `src/stylesheets`
  * `globals.scss` (if you follow the example configuration below)
* `src/templates`
  * `layouts/`
  * `**/`
  * {filename}.html/.hbs

### Configuration
Copy the `scripts` and `config` object from bellow into your `package.json` file. Adapt the config as required. The initial configuration for all tasks is found in `node_modules/@viu/launchpad/gulpfile.mjs/config.json` and a working demo config can be found in `demo/package.json`.

```json
  "scripts": {
    "start": "npm run develop",
    "develop": "npm run gulp",
    "gulp": "gulp --gulpfile node_modules/@viu/launchpad/gulpfile.mjs/index.mjs",
    "lint": "npm run gulp lint",
    "production": "npm run gulp production",
    "demo": "npm run gulp production && npm run gulp server"
  },
  "config": {
    "root": { /* allows you to specify your own source and destination folders. the following values are defaults and can be omitted in your file, unless you want to change them */
      "base": "../../../../",
      "src": "../../../../src",
      "dest": "../../../../public"
    },
    "tasks": {
      "js": {
        "entries": {
          "global": [
            "./global.js"
          ]
        },
        "plugins": [
          {
            "reference": "jQuery",
            "name": "jquery"
          }
        ]
      },
      "nunjucks": {
        "src": "./templates/**/*.html",
        "extensions": [
          "html",
          "json"
        ],
        "excludeFolders": [
          "layouts",
          "macros",
          "partials"
        ],
        "componentExtensions": ["html"]
      },
      "handlebars": {
        "src": "./templates/**/*.hbs",
        "destExtension": "html"
      },
      "browserSync": {
        "https": false,
        "open": false,
        "cors": true,
        "rewrites":  { "from": ".", "to": "/index.hbs"} /* for projects that use push-state and need to send the same html file for all requests */
      },
      "production": {
        "rev": false
      }
    }
  }
```

### Modes

#### Development (Default)
```bash
npm start
```
The `npm start` command runs the default task, defined in gulpfile.mjs/tasks/default.js. All files will compile in development mode (uncompressed with source maps). [BrowserSync](http://www.browsersync.io/) will serve up files to `localhost:3000` and will stream live changes to the code and assets to all connected browsers. Additional BrowserSync tools are available on `localhost:3001`. The `npm run develop` command has the same effect.
*Launchpad can only be run within a project. (ex. demo)*

#### Linting
```bash
npm run lint
```
Runs linters on JavaScript and TypeScript (with [eslint](http://eslint.org)) and CSS, SCSS (with [stylelint](https://stylelint.io/)) and outputs warnings and errors according to the specified linter config. By default the subfolders `vendor` and `generated` will be excluded and files in there will not be checked. 

The eslint-config inherits from the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) and adds the environments for `browser` and `node`. Additionally the eslint-plugin `import` is installed to check failed imports.

CSS and SCSS is linted via stylelint. The default config extends [the sass guidelines](https://github.com/bjankord/stylelint-config-sass-guidelines) and adds stricter configuration for classic BEM conventions. Feel free to configure according to your project's specifications.

#### Production
```bash
npm run production
```
This will compile revisioned and compressed files to `./public`.

#### Demo
```bash
npm run demo
```
In addition to generating production files, this will start a static server to serve the files from http://localhost:5000. This is primarily meant as a way to preview your production build locally, not for use as a live production server.

#### Debug
```bash
npm run debug
```
This will compile and revision files to `./public`, in the same way that `npm run production` does. However, this command will not minify / uglify your code, but rather keep the readable source.

## Features

### JS

Modular ES2021 and/or Typescript with [Babel](http://babeljs.io/) and [Webpack](http://webpack.github.io/).

Also:
  - Async requires
  - Multiple bundles
  - Shared modules
  - Source Maps

### CSS

[Sass](http://sass-lang.com/) (indented, SCSS, or both) with JSON importer and autoprefixer based on browserlist.

### HTML / Data

Static templating with either [Nunjucks](https://mozilla.github.io/nunjucks/) or [Handlebars](https://github.com/handlebars-lang/handlebars.js) and [Webpack](http://webpack.github.io/).

Components automatically load the `json` datafile (in the same folder and with the same name as the component) and combine this data object with data that can be provided directly in the template where the component is included. Data provided via the layout is overwriting the default data taken from the component folder.

Components are used via the component-tag which is not Nunjucks standard functionality but added in VIU Launchpad. usage:
```
{% component 'test', exampleComponentData %}
```

Components can also be used from within node modules. For that to work, use the following format:
```
{% component '::test', exampleComponentData %}
```

For this to work, the node module needs to be named "test" and it needs to contain an 'index.html' and an 'index.json' file (make sure to list these files in the files-property of the packages.json).
To be able to use macros from node packages, reference them with their path: {% import '../node_modules/example-component/index.html' as example %} and then call as with other macros.

You may configure this task to use gulp-cached to only rebuild, if the file has actually changed during development.

In projects with many html templates, this  may improve performance. As a side-effect, the templates will not be rebuilt, if only the json is edited.

```
"html": {
  "cache": true,
},
```

The data/global.json and the package.json are merged recursively with the page template of the current page and the merged object is available in the template.

#### How to Use Handlebars

To use the handlebars task, just create `.hbs` files in your components / layouts.

The handlebars task can either be run in parallel to the html task (the handlebar task only picks up `*.hbs` files), or can completely replace the html task.
The demo project shows the tasks running in parallel.

To disable the `nunjucks` task, update the package.json:
`
  "config": {
    "tasks": {
      "nunjucks": false,
      // ...
    }
  }
`

To disable the `handlebars` task, update the package.json:
`
  "config": {
    "tasks": {
      "handlebars": false,
      // ...
    }
  }
`

### Images

Compression with imagemin (based on mozjpeg)

### SVG Sprites
```
gulpfile.mjs/tasks/svgSprite
```
SVGs sprites are super powerful. This particular setup allows styling 2 different colors from your CSS. You can have unlimited colors hard coded into your SVG.  

In the following example, the first path will be `red`, the second will be `white`, and the third will be `blue`. Paths **without a fill attribute** will inherit the `fill` property from CSS. Paths with **fill="currentColor"** will inherit the current CSS `color` value, and hard-coded fills will not be overwritten, since inline styles trump CSS values.

```sass
.sprite
  fill: red
  color: white
```

```svg
  <svg xmlns="http://www.w3.org/2000/svg">
    <path d="..."/>
    <path fill="currentColor" d="..."/>
    <path fill="blue" d="..."/>
  </svg>
```

We've included a helper to generate the required SVG markup in `src/templates/macros/helpers.html`, so you can do:
```html
  {{ sprite('my-icon') }}
```
Which generates HTML-output like this:

```html
  <span class="sprite sprite--my-icon">
    <svg viewBox="0 0 500 500"><use xlink:href=#svgicon-my-icon" /></use></svg>
  </span>
```

Feel free to update the helper to your liking and add additional classes etc.

We recommend setting up your SVGs on a square canvas, centering your artwork, and expanding/combining any shapes of the same color. This last step is important.

The IDs of the generated sprite-symbol-references will be prefixed with `svgicon-` (configurable in the task svgSprite-config). It is recommend to always use a prefix for the sprite-icons, to prevent issues with ID-collision in HTML (as an example: there could be an input-field with the id `email` and also an icon `email`—they both would get the same id, which is bad).

### Static Files (favicons, app icons, etc.)
There are some files that belong in your root destination directory that you won't want to process or revision in production. Things like [favicons, app icons, etc.](http://realfavicongenerator.net/), should go in `src/static`, and will get copied over to `public` as a last step (after revisioning in production). *Nothing* should ever go directly in `public`, since it gets completely trashed and re-built when running the `default` or `production` tasks.

### Webapp Files (service workers, .htaccess)
There are some files that belong in your root destination directory that you want to go into webroot and be updated with revisioned URLs. Files like service workers, web workers, your manifest or .htaccess configuration should go in `src/webapp` and will get copied over to `public`. *Nothing* should ever go directly in `public`, since it gets completely trashed and re-built when running the `default` or `production` tasks.

## Target specific variables
Add capability to define build-target-specific variables. These variables can be used in JavaScript files and will be replaced to the configured value at compile-time.

`config.tasks.js.targetBuildVariables`
Usage:
1. List the build-target-specific variables you like to use in your scripts, in tasks.js.targetBuildVariables
2.A Define new npm scripts for all the target-environments you like to have, Environment-Variables to a specific value.
or: 
2.B Set the Environment-Variables (E.g. 'API_USERS') directly via CLI (platform-specific command)
3. Use the variables in your JavaScript files. Note that 'API_USERS' will be available as __API_USERS__ in your Script.

Basic scenario would be that you want to have different build, which test different API's, for instance:
Test-API on: "test.example.com"
Develop-API on: "dev.example.com"
Example package.JSON: (Only with specific 'develop' script. You probably have to define the other Scripts like 
"production_test", "production_develop" etc. as well.)
```
    "tasks": {
      "js": {
        "targetBuildVariables": [
          "API_USERS",
          "API_PRODUCTS"
        ]
      }
    },
    "scripts": {
       "develop": "gulp --gulpfile node_modules/@viu/launchpad/gulpfile.mjs/index.mjs",
       "develop_test": "cross-env API_USERS=test.example.com/getusers API_PRODUCTS=test.example.com/getproducts npm run develop",
       "develop_dev": "cross-env API_USERS=dev.example.com/getusers API_PRODUCTS=dev.example.com/getproducts npm run develop"
    }
```

**IMPORTANT:** Note the `cross-env` call at the beginning of the npm-scripts `develop_test` and `develop_dev`. The `cross-env` package will make sure that setting the environment-variable (e.g. `API_USERS`) will work cross-platform.

With this configuration, you can use your custom variables in your JavaScript files like following:

```
console.log(__API_USERS__);
console.log(__API_PRODUCTS__);
```

```
npm run develop_test
=> 'test.example.com/getusers'
=> 'test.example.com/getproducts'
``` 

```
npm run develop_dev
=> 'dev.example.com/getusers'
=> 'dev.example.com/getproducts'
```
Note: If you have set the environment variable directly via CLI, you would run 'npm run develop'.

## Release Notes / Breaking Changes
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/viu/launchpad/tags).

* 5.0.0: Launchpad Release 5
  * Updated to Gulp5 incl. an almost complete rewrite to MJS (ES 2023)
  * Drop support for Node > 18
  * Switch from node-sass to dart-sass
* 4.5.0: Launchpad Release 4.5
  * Added support for Node 20 LTS / NPM 10
  * Added support for Node 22
* 4.4.0: Launchpad Release 4.4
  * Moved to external sourcemaps for debug to improve loading speed on test environments
* 4.3.0: Launchpad Release 4.3
  * Added dump helper to handlebars
  * Add support for Node 18 LTS / NPM 9
  * Dependency updates to latest versions
  * Fix missing postcss-scss dependency when running lint in consuming projects 
* 4.2.0: Launchpad Release 4.2
  * Debug will once again not minimize css
  * Dependency updates to latest versions
* 4.1.0: Launchpad Release 4.1
  * Debug will once again not minimize javascript and will include inline source maps
  * Dependency updates to latest versions
* 4.0.0: Launchpad Release 4.0
  * Based on Node 14/16 LTS / NPM 8
  * Updated to Webpack 5, ESLint 8, CSS Nano 5
  * Dart-Sass replaced Node-Sass, Stylelint replaced Sass-Lint, ESLint replaced TSLint
  * Dropped autogeneration of icon fonts. Use inline SVG.
  * New features and fixes
    * Nunjucks and Handlebars now fully configurable, equal renderers for HTML
    * Support for TypeScript 4.6 and EcmaScript 2020 including dynamic imports/exports
* 3.2.1: Launchpad Release 3.2.1
  * Improved error logging (especially significant for typescript compilation)
  * Reverted unnecessary engine requirements
  * Add sane defaults to all tasks to not watch temporary extensions, as per [Gulp-Watch #238](https://github.com/floatdrop/gulp-watch/issues/238)
* 3.2.0: Added handlebars support
  * Added support for handlebars templates using the `.hbs` extension.
  * Added support for Node 14 LTS.
* 3.1.0: Config cleanup
  * Moved to JS for eslint config in base and `demo`. Fewer rules exceptions where possible.
  * Streamlined npm scripts available in `demo`
  * Fixed default eslint config referred to from `config.json`. Minor updates to tslint config.
  * Updated dependencies (no majors).
* 3.0.0: Launchpad Release 3.0
  * Based on Node 12 LTS / NPM 6
  * Updated to Gulp 4, Babel 7, Webpack 4, MozJPEG and more. Refactored all gulp tasks
  * New features and fixes
    * Support for TypeScript 3.7 and EcmaScript 2020 including dynamic imports/exports
    * Config now allows regular expressions when defining loaders for webpack
    * SVG-icons ids are now prefixed with `svgicon-` (configurable) to prevent id collisions with other HTML-elements
    * Autoprefixer and cssnano have replace gulp-autoprefixer and gulp-cssnano. This means less gulp-dependencies and thus more up-to-date code. Added gulp-postcss to process both autoprefixer and cssnano at the same time.
    * Fixed revision task. HTML files where revisioned (shouldn't) and CSS files weren't (but should)
    * Excludes (for linting and html tasks) are working across all platforms
  * Removed features
    * Dropped all code for automated tests as many launchpad users reported to not use it. They don't use automated testing (sadly) or build their own extensive test suites (fantastic!). Feel free to build your own, Launchpad won't get in the way.
    * Removed HTML linting as it is not widely practiced in general and none of the known launchpad projects ever used it.
    * Removed optional custom development and production tasks as none of the known launchpad projects used any.
    * Removed design system from launchpad but added it to demo project (where it is actually used) as a starting point.
  * Improvements to documentation and code quality
    * Introduced modern (ES2020) linting to the project and removed any linting issues in launchpad and the demo
    * Introducing /src/ folder for `demo` and in documentation as it has become the de-facto default for all projects at viu and for many other such build systems
    * Updated `browserlist.rc` for `/demo` to be based on the widely accepted default
* 2.4.0: Sass includePaths
  * Fixes a bug where includePaths were not prefixed with project path and didn't work in libSass
  * Adds ./node_modules as default includePath as is generally expected of libSass
* 2.3.0: Separate design definition from design implementation
  * Adds possibility to have the design live in json files
  * Adds json to sass parsing
* 2.2.0: Components from multiple node_modules
  * New componentFolders config allows components to be stored in multiple folders e.g. one local src/components folder and one or many folders in node_modules.
* 2.1.0: Process arguments available in Nunjucks data.json
  * The HTML task will expose all arguments in am npm run ... statement to Nunjucks via an {{ args }} object. Example: npm run develop -- --myarg=hello -x 15 can now be referenced in Nunjucks as {{ args.myarg }} and {{ args.x }}.
* 2.0.3 - 2.0.6: Components from node_modules & housekeeping
  * Components can be imported as node_modules, see [HTML](#html) documentation.
  * Node 10.0.0 & npm 6.0.0 have been tested and work.
  * Minor dependency updates.
* 2.0.0 - 2.0.2: Release 2.0 brings fundamental (breaking) updates to VIU Launchpad
  * Based on Node 8 LTS / NPM 5 
  * All major dependencies updated. Webpack updated to v3.10. See [webpack 3 release notes](https://medium.com/webpack/webpack-3-official-release-15fd2dd8f07b) for how to configure webpack in order to improve JavaScript performance.
  * Improved support for TypeScript v2.7. Including TSLint.
  * Switched to babel-preset-env
  * Updated `/demo` project
* 1.0.0: VIU is proud to release version 1.0.0 of the VIU Launchpad. Highlights are:
  * Starting from our first 'stable' release, we will be following strict SemVer naming conventions.
  * Therefore we will not have any braking changes without clear labelling.
  * This release is meant for Node LTS 6.*.

## Authors
* **Michael Keller** - *Maintainer* - [VIU](https://www.viu.ch)
* **Andreas Nebiker** - *Maintainer* - [VIU](https://www.viu.ch)
* **Raphael Ochsenbein** - *Maintainer* - [VIU](https://www.viu.ch)
* **Steffen Rademacker** - *Contributor* - [Webgefrickel](https://www.webgefrickel.de/)
* The **Team at VIU** - *Contributors* - [VIU](https://www.viu.ch)

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments
* Originally based on the popular [GulpStarter](https://github.com/vigetlabs/gulp-starter) (now 'Blendid') by Viget
* Thank you, dear customers. We use launchpad in many of our projects. It is fuel for - and the result of - great things built together with you. 
* Media Credit: camera.mp4 from (http://www.beachfrontbroll.com/) is used in our `./demo/` project
