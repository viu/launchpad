// eslint-disable-next-line no-undef
module.exports = {
  root: true,
  extends: [
    'airbnb-base',
  ],
  env: {
    browser: true,
    node: true,
  },
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'import',
  ],
  parserOptions: {
    ecmaVersion: 12, // ES2021
  },
  rules: {
    'import/named': 2,
  },
};
