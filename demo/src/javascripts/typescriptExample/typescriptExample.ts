/**
 * A sample class
 */
class Greeter {
  /**
   * this is a sample, private property
   */
  private greeting: string;

  constructor(message: string) {
    this.greeting = message;
  }

  /**
   * A sample method
   */
  public greet() {
    // eslint-disable-next-line no-console
    console.log(this.greeting);
  }
}

export default Greeter;
