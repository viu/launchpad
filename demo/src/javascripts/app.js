import './asyncModules';
import exclaimify from './exclaimify';
import Greeter from './typescriptExample/typescriptExample.ts';

const button = document.getElementById('button');

// Dynamic imports:
// https://github.com/tc39/proposal-dynamic-import
const alertAsyncMessage = function asyncAlert() {
  // Note: the webpackChunkName comment should define a bundle name.
  // We have defined the following pattern for chunk-files in webpackConfig.output:
  // '[name]-chunk.js';
  // This should result in the bundle name: 'asyncMessage-chunk.js'.
  // Currently it does result in 1-chunk.js. This sould be fixed.
  import(/* webpackChunkName: 'asyncMessage' */ './asyncMessage')
    .then((module) => {
      alert(exclaimify(module.default)); // eslint-disable-line
    });
};

button.addEventListener('click', alertAsyncMessage);

const greeter = new Greeter('Hello, Typescript!');
greeter.greet();

// SVG Sprite loading
// only loads the svg if the dom has been fully loaded
let cachedSvgSprite = null;

function insertSvg(svgSprite) {
  const loadedSvgSprite = svgSprite || cachedSvgSprite;
  if (document.readyState !== 'loading' && loadedSvgSprite) {
    // place the svg into the dom if it's ready
    document.body.insertBefore(loadedSvgSprite, document.body.childNodes[0]);
    cachedSvgSprite = null;
  } else if (svgSprite) {
    // cache sprite until the dom is ready
    cachedSvgSprite = svgSprite;
  }
}

// load svg icon sprites async and place them in dom for older browsers
const spriteRequest = new XMLHttpRequest();
spriteRequest.addEventListener('load', function placeInDOM() {
  const div = document.createElement('div');
  div.style.height = '0px';
  div.style.width = '0px';
  div.innerHTML = this.responseText;
  insertSvg(div);
});

spriteRequest.open('GET', '/images/sprites.svg', true);
spriteRequest.send();
