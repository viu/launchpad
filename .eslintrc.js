module.exports = {
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 2023, // ES2023
  },
  rules: {
    'import/no-dynamic-require': 0,
    'import/extensions': ['error', 'ignorePackages', {
      js: 'never',
      mjs: 'always',
    }],
  },
  overrides: [
    {
      files: ['*.mjs'],
      parserOptions: {
        sourceType: 'module',
      },
    },
    {
      files: ['*.js'],
      parserOptions: {
        sourceType: 'script',
        globalReturn: true,
      },
    },
  ],
};
