import browserSync from 'browser-sync';
import gulpif from 'gulp-if';
import changed from 'gulp-changed';
import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import path from 'path';
import config from '../lib/configReader.mjs';

function images(callback) {
  if (!config.tasks.images) {
    return callback();
  }

  const paths = {
    src: path.posix.join(config.root.src, config.tasks.images.src, `/**/*.{${config.tasks.images.extensions}}`),
    dest: path.posix.join(config.root.dest, config.tasks.images.dest),
  };

  return gulp
    .src([paths.src, '*!README.md'], { encoding: false })
    .pipe(changed(paths.dest)) // Ignore unchanged files
    .pipe(gulpif(config.tasks.images.imagemin, imagemin())) // Optimize
    .pipe(gulp.dest(paths.dest))
    .pipe(browserSync.stream());
}

export default images;
