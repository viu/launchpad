// Copies files from one folder to another
// Unlike the static task, this task is run
// before revving takes place and thus the
// files in this task are included in gulp-rev

import gulp from 'gulp';
import path from 'path';
import config from '../lib/configReader.mjs';

function webapp(callback) {
  if (!config.tasks.webapp) {
    return callback();
  }

  const paths = {
    src: [
      path.posix.join(config.root.src, config.tasks.webapp.src, '/**/*'),
      // since gulp.src does not conside /**/* to be 'all folders and their files' but
      // only 'all folders and their non-.hidden files. We add a second task to copy
      // .hidden files.
      path.posix.join(config.root.src, config.tasks.webapp.src, '/**/.*'),
    ],
    dest: path.posix.join(config.root.dest, config.tasks.webapp.dest),
  };

  return gulp
    .src(paths.src, { encoding: false })
    .pipe(gulp.dest(paths.dest));
}

export default webapp;
