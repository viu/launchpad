import gulp from 'gulp';
import gulpif from 'gulp-if';
import browserSync from 'browser-sync';
// importing legacy api as importers don't work
// with new gulp-sass v6 default api
// eslint-disable-next-line import/extensions
import gulpSass from 'gulp-sass/legacy.js';
import * as sassLib from 'sass';
import jsonImporter from 'node-sass-json-importer';
import sourcemaps from 'gulp-sourcemaps';
import path from 'path';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import postcss from 'gulp-postcss';
import handleErrors from '../lib/handleErrors.mjs';
import projectPath from '../lib/projectPath.mjs';
import config from '../lib/configReader.mjs';

const sass = gulpSass(sassLib);

function css(callback) {
  if (!config.tasks.css) {
    return callback();
  }

  const paths = {
    src: path.posix.join(config.root.src, config.tasks.css.src, `/**/*.{${config.tasks.css.extensions}}`),
    dest: path.posix.join(config.root.dest, config.tasks.css.dest),
  };

  const sassLoaderConfig = {
    importer: jsonImporter(),
  };

  // update includepaths to be absolute
  if (config.tasks.css.sass && config.tasks.css.sass.includePaths) {
    config.tasks.css.sass.includePaths = config.tasks.css.sass.includePaths.map(
      (includePath) => projectPath(includePath),
    );
  }

  const sassConfig = Object.assign(
    config.tasks.css.sass,
    sassLoaderConfig,
  );

  const postcssConfig = {};
  const postcssPlugins = [
    autoprefixer(),
  ];
  if (global.production) {
    postcssPlugins.push(cssnano(config.tasks.css.min));
  }

  return gulp
    .src(paths.src)
    .pipe(gulpif(!global.production, sourcemaps.init()))
    .pipe(sass(sassConfig)).on('error', handleErrors)
    .pipe(postcss(postcssPlugins, postcssConfig))
    .pipe(gulpif(!global.production, sourcemaps.write()))
    .pipe(gulp.dest(paths.dest))
    .pipe(browserSync.stream());
}

export default css;
