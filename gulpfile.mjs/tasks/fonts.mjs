import browserSync from 'browser-sync';
import changed from 'gulp-changed';
import gulp from 'gulp';
import path from 'path';
import config from '../lib/configReader.mjs';

function fonts(callback) {
  if (!config.tasks.fonts) {
    return callback();
  }

  const paths = {
    src: path.posix.join(config.root.src, config.tasks.fonts.src, `/**/*.{${config.tasks.fonts.extensions}}`),
    dest: path.posix.join(config.root.dest, config.tasks.fonts.dest),
  };

  return gulp
    .src([paths.src, '*!README.md'], { encoding: false })
    .pipe(changed(paths.dest)) // Ignore unchanged files
    .pipe(gulp.dest(paths.dest))
    .pipe(browserSync.stream());
}

export default fonts;
