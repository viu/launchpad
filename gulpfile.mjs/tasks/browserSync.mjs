import browserSync from 'browser-sync';
import webpack from 'webpack';
import merge from 'merge';
import path from 'path';
import historyApiFallback from 'connect-history-api-fallback';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import config from '../lib/configReader.mjs';
import webpackMultiConfig from '../lib/webpack-multi-config.mjs';
import normalisePath from '../lib/normalisePath.mjs';

function browserSyncTask(callback) {
  if (global.production) callback();

  const browserSyncConfig = merge(true, config.tasks.browserSync, {});
  browserSyncConfig.server.baseDir = browserSyncConfig.server.baseDir.map(
    (b) => path.resolve(process.cwd(), path.posix.join(config.root.dest, b)),
  );

  const webpackConfig = webpackMultiConfig('development');
  const compiler = webpack(webpackConfig);

  let proxy = browserSyncConfig.proxy || null;
  if (typeof (proxy) === 'string') {
    // eslint-disable-next-line no-multi-assign
    browserSyncConfig.proxy = proxy = {
      target: proxy,
    };
  }

  const server = proxy || browserSyncConfig.server;
  server.middleware = [
    webpackDevMiddleware(compiler, {
      stats: 'errors-only',
      publicPath: normalisePath('/', webpackConfig.output.publicPath),
    }),
    webpackHotMiddleware(compiler),
    (req, res, next) => {
      res.setHeader('Access-Control-Allow-Origin', '*');
      next();
    },
  ];

  if (config.tasks.browserSync.rewrites) {
    const rewrites = [];
    for (let i = 0; i < config.tasks.browserSync.rewrites.length; i += 1) {
      const rewrite = config.tasks.browserSync.rewrites[i];
      rewrite.from = new RegExp(rewrite.from, 'i');
      rewrites.push(rewrite);
    }

    server.middleware.push(historyApiFallback({
      rewrites,
    }));
  }

  browserSync.init(browserSyncConfig);
  callback();
}

export default browserSyncTask;
