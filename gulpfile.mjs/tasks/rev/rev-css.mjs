// Rev and compress CSS and JS files (this is done after assets, so that if a
// referenced asset hash changes, the parent hash will change as well

import gulp from 'gulp';
import path from 'path';
import rev from 'gulp-rev';
import revNapkin from 'gulp-rev-napkin';
import config from '../../lib/configReader.mjs';

function revCSS() {
  return gulp
    .src(path.posix.join(config.root.dest, config.tasks.css.dest, '/**/*.css'))
    .pipe(rev())
    .pipe(gulp.dest(path.posix.join(config.root.dest, config.tasks.css.dest)))
    .pipe(revNapkin({ verbose: false, force: true }))
    .pipe(rev.manifest(path.posix.join(config.root.dest, 'rev-manifest.json'), { merge: true }))
    .pipe(gulp.dest('./'));
}

export default revCSS;
