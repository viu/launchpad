// update asset references with reved filenames in compiled css + js

import gulp from 'gulp';
import path from 'path';
import revReplace from 'gulp-rev-replace';
import gulpIf from 'gulp-if';
import config from '../../lib/configReader.mjs';

function revUpdateReferences() {
  const manifest = gulp.src(path.posix.join(config.root.dest, 'rev-manifest.json'));

  return gulp
    .src(path.posix.join(config.root.dest, '/**/*.{css,js}'))
    .pipe(gulpIf(config.tasks.production.rev, revReplace({ manifest })))
    .pipe(gulp.dest(config.root.dest));
}

export default revUpdateReferences;
