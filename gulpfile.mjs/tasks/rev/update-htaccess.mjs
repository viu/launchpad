// Update asset references in application config files

import gulp from 'gulp';
import revReplace from 'gulp-rev-replace';
import path from 'path';
import gulpIf from 'gulp-if';
import rename from 'gulp-rename';
import config from '../../lib/configReader.mjs';

function updateHtaccess() {
  const manifest = gulp.src(path.posix.join(config.root.dest, '/rev-manifest.json'));
  // eslint-disable-next-line max-len
  const htmlDest = config.tasks.nunjucks ? config.tasks.nunjucks.dest : config.tasks.handlebars.dest;

  return gulp
    .src(path.posix.join(config.root.dest, htmlDest, '.htaccess'), { allowEmpty: true })
    // [1] usually, one should be able to declare '.htaccess' to be a file type considered
    // in rev-replace. e.g. revReplace({manifest, replaceInExtensions: ['.htaccess']})
    // unfortunately, .htaccess seems to be a very particular beast thus we first rename
    // the .htaccess into a js file and then back.
    .pipe(rename(path.posix.join(config.root.dest, htmlDest, 'htaccess.js')))
    .pipe(gulpIf(config.tasks.production.rev, revReplace({ manifest })))
    // [1]
    .pipe(rename(path.posix.join(config.root.dest, htmlDest, '.htaccess')))
    .pipe(gulp.dest(path.posix.join(config.root.dest, htmlDest)));
}

export default updateHtaccess;
