// Update asset references in HTML

import gulp from 'gulp';
import revReplace from 'gulp-rev-replace';
import path from 'path';
import gulpIf from 'gulp-if';
import config from '../../lib/configReader.mjs';

function updateHTML() {
  const manifest = gulp.src(path.posix.join(config.root.dest, '/rev-manifest.json'));
  const htmlDest = config.tasks.nunjucks
    ? config.tasks.nunjucks.dest
    : config.tasks.handlebars.dest;

  return gulp
    .src(path.posix.join(config.root.dest, htmlDest, '/**/*.html'))
    .pipe(gulpIf(config.tasks.production.rev, revReplace({ manifest })))
    .pipe(gulp.dest(path.posix.join(config.root.dest, htmlDest)));
}

export default updateHTML;
