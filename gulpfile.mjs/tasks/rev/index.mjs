import gulp from 'gulp';
import config from '../../lib/configReader.mjs';
import revAssets from './rev-assets.mjs';
import revUpdateReferences from './rev-update-references.mjs';
import revCSS from './rev-css.mjs';
import updateHTML from './update-html.mjs';
import updateStatic from './update-static.mjs';
import updateHtaccess from './update-htaccess.mjs';

function revision(callback) {
  // there is no non-functional way to skip the execution of a task
  // see: https://github.com/gulpjs/undertaker/issues/57
  if (!config.tasks.production.rev) {
    return callback();
  }

  return gulp.series(
    // 1) Add md5 hashes to assets referenced by CSS and JS files
    revAssets,
    // 2) Update asset references (images, fonts, etc) with reved filenames in compiled css + js
    revUpdateReferences,
    // 3) Rev and compress CSS and JS files (this is done after assets,
    // so that if a referenced asset hash changes, the parent hash will change as well
    revCSS,
    // 4) Update asset references in HTML
    updateHTML,
    // 5) Update asset references in serviceworkers, manifests and other webapp files in webroot
    updateStatic,
    // 6) Update asset references in .htaccess
    updateHtaccess,
  )(callback);
}

export default revision;
