import gulp from 'gulp';
import path from 'path';
import rev from 'gulp-rev';
import revNapkin from 'gulp-rev-napkin';
import gulpIf from 'gulp-if';
import config from '../../lib/configReader.mjs';

// 1) Add md5 hashes to assets referenced by CSS and JS files
// Ignore files that may reference assets. We'll rev them next.
function revAssets() {
  return gulp
    .src(path.posix.join(config.root.dest, '/**/*.!(css|js|json|html|*.html)'), { encoding: false })
    .pipe(gulpIf(config.tasks.production.rev, rev()))
    .pipe(gulp.dest(config.root.dest))
    .pipe(revNapkin({ verbose: false, force: true }))
    .pipe(rev.manifest(path.posix.join(config.root.dest, 'rev-manifest.json'), { merge: true }))
    .pipe(gulp.dest('./'));
}

export default revAssets;
