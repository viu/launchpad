import compress from 'compression';
import express from 'express';
import logger from 'morgan';
import open from 'open';
import path from 'path';
import log from 'fancy-log';
import colors from 'ansi-colors';
import config from '../lib/configReader.mjs';

const destination = config.root?.dest || '/dist';
const settings = {
  root: path.resolve(process.cwd(), destination),
  port: process.env.PORT || 5500,
  logLevel: process.env.NODE_ENV ? 'combined' : 'dev',
  staticOptions: {
    extensions: ['html'],
    maxAge: '31556926',
  },
};

function server() {
  const url = `http://localhost:${settings.port}`;

  express()
    .use(compress())
    .use(logger(settings.logLevel))
    .use('/', express.static(settings.root, settings.staticOptions))
    .use('/', express.static(`${settings.root}/templates`, settings.staticOptions))
    .listen(settings.port);

  log(`production server started on ${colors.green(`${url} with root: ${settings.root}`)}`);
  open(url);
}

export default server;
