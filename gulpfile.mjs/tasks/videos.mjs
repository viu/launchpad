import browserSync from 'browser-sync';
import changed from 'gulp-changed';
import gulp from 'gulp';
import path from 'path';
import config from '../lib/configReader.mjs';

function videos(callback) {
  if (!config.tasks.videos) {
    return callback();
  }

  const paths = {
    src: path.posix.join(config.root.src, config.tasks.videos.src, `/**/*.{${config.tasks.videos.extensions}}`),
    dest: path.posix.join(config.root.dest, config.tasks.videos.dest),
  };

  return gulp
    .src([paths.src, '*!README.md'], { encoding: false })
    .pipe(changed(paths.dest)) // Ignore unchanged files
    .pipe(gulp.dest(paths.dest))
    .pipe(browserSync.stream());
}

export default videos;
