import gulp from 'gulp';
import clean from './clean.mjs';
import webapp from './webapp.mjs';
import staticFiles from './static.mjs';
import getEnabledTasks from '../lib/getEnabledTasks.mjs';

const tasks = getEnabledTasks('debug');

export default gulp.series(
  clean,
  gulp.parallel(...tasks.assetTasks),
  gulp.parallel(...tasks.codeTasks),
  webapp,
  staticFiles,
);
