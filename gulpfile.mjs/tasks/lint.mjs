import gulp from 'gulp';
import eslint from './eslint.mjs';
import stylelint from './stylelint.mjs';

export default gulp.parallel(eslint, stylelint);
