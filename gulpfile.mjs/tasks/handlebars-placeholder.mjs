/**
 * handlebars helper: {{placeholder Name Data Variation}}
 *
 */
import fs from 'fs';
import ghbs from 'gulp-hb';
import path from 'path';
import merge from 'merge';
import component from './handlebars-component.mjs';
import config from '../lib/configReader.mjs';

function placeholder() {
  try {
    // eslint-disable-next-line prefer-rest-params
    const context = arguments[arguments.length - 1];
    const contextDataRoot = context.data && context.data.root ? context.data.root : {};
    // eslint-disable-next-line prefer-rest-params
    const name = typeof arguments[0] === 'string' ? arguments[0] : context.hash.name;
    // eslint-disable-next-line prefer-rest-params
    const templateFile = typeof arguments[1] === 'string' ? arguments[1] : context.hash.template;
    let placeholderData = {};

    // validate
    if (!name) {
      throw new Error('Placeholder name parameter not set');
    }

    if (!templateFile) {
      throw new Error('Placeholder template parameter not set');
    }

    // data
    // eslint-disable-next-line no-underscore-dangle
    if (contextDataRoot._locals) {
      // eslint-disable-next-line no-underscore-dangle
      placeholderData = merge.recursive(true, placeholderData, contextDataRoot._locals);
    }

    // eslint-disable-next-line no-underscore-dangle
    if (contextDataRoot._query) {
      // eslint-disable-next-line no-underscore-dangle
      placeholderData = merge.recursive(true, placeholderData, contextDataRoot._query);
    }

    if (context.hash) {
      placeholderData = merge.recursive(true, placeholderData, context.hash);
    }

    const templatePath = path.posix.join(
      config.root.src,
      config.tasks.handlebars.placeholders,
      name,
      `${templateFile}.${config.tasks.handlebars.viewFileExtension}`,
    );

    if (fs.existsSync(templatePath)) {
      const html = ghbs.handlebars.compile(
        fs.readFileSync(templatePath, 'utf8'),
      )(placeholderData, context);

      return new ghbs.handlebars.SafeString(html);
    }

    throw new Error(`Placeholder ${templatePath} not found.`);
  } catch (e) {
    return new ghbs.handlebars.SafeString(
      `<p>${e.message}</p>`,
    );
  }
}

// since the handlebars version used by gulp-handlebars is different
// than the version used withing this component helper here, we need register the
// component helper here as well, otherwise we couldn't have recursive
// components.
ghbs.handlebars.registerHelper('component', component);
ghbs.handlebars.registerHelper('placeholder', placeholder);

export default placeholder;
