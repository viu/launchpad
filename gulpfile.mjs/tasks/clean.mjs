// removes destinatoin folder and all
// it's content

import { deleteAsync } from 'del';
import config from '../lib/configReader.mjs';

function clean(callback) {
  return deleteAsync([config.root.dest], { force: true }).then(() => {
    callback();
  });
}

export default clean;
