import browserSync from 'browser-sync';
import gulp from 'gulp';
import imagemin from 'gulp-imagemin';
import rename from 'gulp-rename';
import svgstore from 'gulp-svgstore';
import path from 'path';
import config from '../lib/configReader.mjs';

function svgSprite(callback) {
  if (!config.tasks.svgSprite) {
    return callback();
  }

  const settings = {
    src: path.posix.join(config.root.src, config.tasks.svgSprite.src, '/*.svg'),
    dest: path.posix.join(config.root.dest, config.tasks.svgSprite.dest),
  };

  return gulp.src(settings.src)
    .pipe(imagemin())
    .pipe(rename({ prefix: config.tasks.svgSprite.prefix }))
    .pipe(svgstore())
    .pipe(gulp.dest(settings.dest))
    .pipe(browserSync.stream());
}

export default svgSprite;
