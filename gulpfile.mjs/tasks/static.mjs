// copies static files into the destination folder
import changed from 'gulp-changed';
import gulp from 'gulp';
import path from 'path';
import browserSync from 'browser-sync';
import config from '../lib/configReader.mjs';

const paths = {
  src: [
    path.posix.join(config.root.src, config.tasks.static.src, '/**/*'),
    // since gulp.src does not consider /**/* to be 'all folders and their files' but
    // only 'all folders and their non-.hidden files. We add a second task to copy
    // .hidden files.
    path.posix.join(config.root.src, config.tasks.static.src, '/**/.*'),
  ],
  dest: path.posix.join(config.root.dest, config.tasks.static.dest),
};

function staticFiles() {
  return gulp
    .src(paths.src, { allowEmpty: true, encoding: false })
    .pipe(changed(paths.dest)) // Ignore unchanged files
    .pipe(gulp.dest(paths.dest))
    .on('end', browserSync.reload);
}

export default staticFiles;
