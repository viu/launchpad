import gulp from 'gulp';
import getEnabledTasks from '../lib/getEnabledTasks.mjs';
import revision from './rev/index.mjs';
import clean from './clean.mjs';
import webapp from './webapp.mjs';
import staticFiles from './static.mjs';
import sizeReport from './sizereport.mjs';

function production() {
  global.production = true;

  const tasks = getEnabledTasks('production');

  gulp.series(
    clean,
    gulp.parallel(...tasks.assetTasks),
    gulp.parallel(...tasks.codeTasks),
    webapp,
    revision,
    sizeReport,
    staticFiles,
  )();

  return Promise.resolve();
}

export default production;
