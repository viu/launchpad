import browserSync from 'browser-sync';
import data from 'gulp-data';
import gulp from 'gulp';
import gulpif from 'gulp-if';
import htmlmin from 'gulp-htmlmin';
import rename from 'gulp-rename';
import path from 'path';
import fs from 'fs';
import merge from 'merge';
import minimist from 'minimist';
import hb from 'gulp-hb';
import component from './handlebars-component.mjs';
import dump from './handlebars-dump.mjs';
// import placeholder from './handlebars-placeholder'; // todo: placeholder
import removeBOM from '../lib/removeBOM.mjs';
import handleErrors from '../lib/handleErrors.mjs';
import config from '../lib/configReader.mjs';

const argv = minimist(process.argv.slice(2));

// get data for templates
const getData = function getTemplateData(file) {
  let globalData = {};
  let packageData = {};

  // check for global.json
  const globalDataPath = path.resolve(config.root.src, config.tasks.data.src, 'global.json');
  if (fs.existsSync(globalDataPath)) {
    try {
      globalData = JSON.parse(removeBOM(fs.readFileSync(globalDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }
  // add package json into global data
  const packageDataPath = path.resolve(config.root.base, 'package.json');
  if (fs.existsSync(packageDataPath)) {
    try {
      packageData = JSON.parse(removeBOM(fs.readFileSync(packageDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }
  packageData = { package: packageData };
  globalData = merge.recursive(true, packageData, globalData);

  // add arguments to global data
  globalData.args = argv;

  // find relative data file
  let pageDataPath = path.resolve(config.root.src, config.tasks.data.src, file.relative);

  // replace duplicated folder separator
  pageDataPath = pageDataPath.replace('.hbs', '.json').replace(path.sep + path.sep, path.sep);

  // remove folders specified in config (so we don't need to put data files into /data/templates
  config.tasks.data.removeFromPath.forEach((pathToRemove) => {
    // eslint-disable-next-line no-param-reassign
    pathToRemove = pathToRemove.replace('/', path.sep);
    pageDataPath = pageDataPath.replace(pathToRemove, '');
  });

  // check if file exists - otherwise provide empty data
  let pageData = {};
  if (fs.existsSync(pageDataPath) && !fs.lstatSync(pageDataPath).isDirectory()) {
    try {
      pageData = JSON.parse(removeBOM(fs.readFileSync(pageDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }

  return merge.recursive(true, globalData, pageData);
};

// main task
function handlebars(callback) {
  if (!config.tasks.handlebars) {
    return callback();
  }

  // this solution appears to only be working if more than one entry is placed in excludeFolders
  const exclude = path.normalize(`!/**/{${config.tasks.handlebars.excludeFolders.join(',')}}/**/*`);
  const srcPaths = [config.tasks.handlebars.src, exclude];

  const partials = [];

  for (let i = 0; i < config.tasks.handlebars.partials.length; i += 1) {
    partials.push(path.posix.join(config.root.src, config.tasks.handlebars.partials[i]));
  }

  return gulp
    .src(srcPaths, { cwd: config.root.src, nodir: true })
    .pipe(data(getData))
    .on('error', handleErrors)
    .pipe(hb({ debug: config.tasks.handlebars.debug })
      .partials([
        ...partials,
      ])
      .helpers({
        component,
        dump,
        // placeholder, // todo: placeholder
      }))
    .on('error', handleErrors)
    .pipe(gulpif(global.production, htmlmin(config.tasks.handlebars.htmlmin)))
    // eslint-disable-next-line prefer-arrow-callback
    .pipe(rename((template) => {
      // Updates the object in-place

      // eslint-disable-next-line no-param-reassign
      template.extname = `.${config.tasks.handlebars.destExtension || 'html'}`;
    }))
    .pipe(gulp.dest(path.posix.join(config.root.dest, config.tasks.handlebars.dest)))
    .on('end', browserSync.reload);
}

export default handlebars;
