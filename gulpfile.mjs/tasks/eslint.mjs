import gulp from 'gulp';
// eslint-disable-next-line import/no-unresolved
import eslint from 'gulp-eslint-new';
import path from 'path';
import config from '../lib/configReader.mjs';

function eslintTask(callback) {
  if (!config.tasks.lint.js) {
    return callback();
  }

  // source files are defined with their extensions in this case,
  // because we dont want to try linting typescript files
  const srcs = [];

  // add the srcs and exclude items to the srcs array
  config.tasks.lint.js.extensions.forEach((ext) => {
    config.tasks.lint.js.src.forEach((includeItem) => {
      srcs.push(path.posix.join(includeItem, `/**/*.${ext}`));
    });
    config.tasks.lint.js.exclude.forEach((excludeItem) => {
      srcs.push(`!${path.posix.join(excludeItem, `/**/*.${ext}`)}`);
    });
  });

  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp
    .src(srcs, { cwd: config.root.src })
    .pipe(eslint(config.tasks.lint.js.options))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}

export default eslintTask;
