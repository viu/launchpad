// watches for changes to files of a particular
// extension and, when detected, runs their appropriate
// task(s) and then reloads the browser

import gulp from 'gulp';
import path from 'path';
import watch from 'gulp-watch';
import config from '../lib/configReader.mjs';
import browserSync from './browserSync.mjs';
import getEnabledTasks from '../lib/getEnabledTasks.mjs';

function watchTask(callback) {
  const { watchableTasks } = getEnabledTasks();
  watchableTasks.forEach((task) => {
    const taskConfig = config.tasks[task.name];

    if (taskConfig) {
      let glob = [];
      glob.push(path.posix.join(config.root.src, taskConfig.src, `**/*.{${taskConfig.extensions.join(',')}}`));

      // Ignoring temporary files pattern, as recommended here:
      // https://github.com/floatdrop/gulp-watch/issues/238
      // https://github.com/floatdrop/gulp-watch/issues/242
      if (taskConfig.excludeExtensions) {
        glob.push(`!${path.posix.join(config.root.src, taskConfig.src, `**/*{${taskConfig.excludeExtensions.join(',')}}`)}`);
      }

      if (taskConfig.watchSrc) {
        glob = [];
        taskConfig.watchSrc.forEach((watchPath) => {
          glob.push(path.posix.join(config.root.src, watchPath, `**/*.{${taskConfig.extensions.join(',')}}`));
          if (taskConfig.watchExcludeExtensions) {
            glob.push(path.posix.join(`!${config.root.src}`, watchPath, `**/*{${taskConfig.watchExcludeExtensions.join(',')}}`));
          }
        });
      }

      watch(glob, gulp.series(task));
    }
  });

  callback();
}

export default gulp.series(browserSync, watchTask);
