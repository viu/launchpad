// returns a list of all files in the destination
// folder and their filesize incl. their gzipped filesize
// also includes totals for filesize with and without gzip

import gulp from 'gulp';
import sizereport from 'gulp-sizereport';
import config from '../lib/configReader.mjs';

function sizeReport() {
  return gulp
    .src([`${config.root.dest}/**/*`, '*!rev-manifest.json'])
    .pipe(sizereport({
      gzip: true,
    }));
}

export default sizeReport;
