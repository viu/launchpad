import ghbs from 'gulp-hb';

function dump(context) {
  return JSON.stringify(context);
}

// since the handlebars version used by gulp-handlebars is different
// than the version used withing this component helper here, we need register the
// component helper here as well, otherwise we couldn't have recursive
// components.
ghbs.handlebars.registerHelper('dump', dump);

export default dump;
