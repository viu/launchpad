import webpack from 'webpack';
import webpackMultiConfig from '../lib/webpack-multi-config.mjs';
import logger from '../lib/compileLogger.mjs';
import config from '../lib/configReader.mjs';

function webpackProduction(callback) {
  const webpackConfig = webpackMultiConfig('production');

  if (!config.tasks.js) {
    return callback();
  }

  return webpack(webpackConfig, (err, stats) => {
    logger(err, stats);
    callback();
  });
}

export default webpackProduction;
