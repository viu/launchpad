import gulp from 'gulp';
import stylelint from '@ronilaukkarinen/gulp-stylelint';
import path from 'path';
import config from '../lib/configReader.mjs';

function stylelintTask(callback) {
  if (!config.tasks.lint.sass) {
    return callback();
  }

  // source files are defined with their extensions in this case,
  // because we dont want to try linting typescript files
  const srcs = [];

  // add the srcs and exclude items to the srcs array
  config.tasks.lint.sass.extensions.forEach((ext) => {
    config.tasks.lint.sass.src.forEach((includeItem) => {
      srcs.push(path.posix.join(includeItem, `/**/*.${ext}`));
    });
    config.tasks.lint.sass.exclude.forEach((excludeItem) => {
      srcs.push(`!${path.posix.join(excludeItem, `/**/*.${ext}`)}`);
    });
  });

  return gulp
    .src(srcs, { cwd: config.root.src })
    .pipe(stylelint(config.tasks.lint.sass.options));
}

export default stylelintTask;
