/**
 * handlebars helper: {{pattern PatternName Data Variation}}
 *
 * Usage
 * {{pattern 'button' 'button-fancy'}}
 * {{pattern name='button' data='button-fancy'}}
 *
 * Usage (passing arguments)
 * {{pattern name='button' disabled=true}}
 *
 * Usage (with children)
 * {{#pattern name='button'}}Click Me{{/pattern}}
 * {{#pattern name='button' disabled=true}}Not Clickable{{/pattern}}
 *
 */
import fs from 'fs';
import ghbs from 'gulp-hb';
import path from 'path';
import merge from 'merge';

import config from '../lib/configReader.mjs';

const patternBasePaths = [
  ...(config.tasks.handlebars ? config.tasks.handlebars.componentFolders || [] : []),
];

function getPattern(folder, templateFile, dataFile) {
  let pattern = null;

  // search base pattern
  patternBasePaths.forEach((patternBasePath) => {
    if (!pattern) {
      const templateFilePath = path.posix.join(
        config.root.src,
        patternBasePath,
        '/',
        folder,
        '/',
        `${templateFile}.${config.tasks.handlebars.viewFileExtension}`,
      );
      const jsonFilePath = path.posix.join(
        config.root.src,
        patternBasePath,
        '/',
        folder,
        '/',
        `${dataFile}.json`,
      );

      if (fs.existsSync(templateFilePath)) {
        pattern = {
          templateFilePath,
          jsonFilePath,
        };
      }
    }
  });

  return pattern;
}

function component() {
  try {
    // eslint-disable-next-line prefer-rest-params
    const context = arguments[arguments.length - 1];
    const contextDataRoot = context.data && context.data.root ? context.data.root : {};
    // default pattern data from controller & view
    // eslint-disable-next-line prefer-rest-params
    const name = typeof arguments[0] === 'string' ? arguments[0] : context.hash.name;
    const folder = name.replace(/[^A-Za-z0-9-]/g, '');
    const templateFile = context.hash && context.hash.template
      ? context.hash.template : folder.toLowerCase();

    let dataFile = folder.toLowerCase(); // default data file
    let passedData = null; // passed data to pattern helper
    let patternData = {}; // collected pattern data

    if (arguments.length >= 3) {
      // eslint-disable-next-line prefer-rest-params
      switch (typeof arguments[1]) {
        case 'string':
          // eslint-disable-next-line prefer-rest-params
          dataFile = arguments[1].replace(/\.json$/i, '').toLowerCase();
          break;
        case 'object':
          // eslint-disable-next-line prefer-rest-params
          passedData = merge.recursive(true, passedData, arguments[1]);
          break;
        case 'number':
        case 'boolean':
          // eslint-disable-next-line prefer-rest-params,prefer-destructuring
          passedData = arguments[1];
          break;
        default:
          break;
      }
    }
    if (context.hash && context.hash.data) {
      switch (typeof context.hash.data) {
        case 'string':
          dataFile = context.hash.data.replace(/\.json$/i, '').toLowerCase();
          break;
        case 'object':
          passedData = merge.recursive(true, passedData, context.hash.data);
          break;
        case 'number':
        case 'boolean':
          passedData = context.hash.data;
          break;
        default:
          break;
      }
    }

    const pattern = getPattern(folder, templateFile, dataFile);

    if (pattern) {
      try {
        // eslint-disable-next-line no-underscore-dangle
        if (contextDataRoot._locals) {
          // eslint-disable-next-line no-underscore-dangle
          patternData = merge.recursive(true, patternData, contextDataRoot._locals);
        }

        if (passedData) {
          patternData = merge.recursive(true, patternData, passedData);
        } else if (fs.existsSync(pattern.jsonFilePath)) {
          const file = fs.readFileSync(pattern.jsonFilePath, 'utf8');
          const data = JSON.parse(file);
          patternData = merge.recursive(true, patternData, data);
        }

        // eslint-disable-next-line no-underscore-dangle
        if (contextDataRoot._query) {
          // eslint-disable-next-line no-underscore-dangle
          patternData = merge.recursive(true, patternData, contextDataRoot._query);
        }

        // Add attributes e.g. "disabled" of {{pattern "button" disabled=true}}
        if (context.hash) {
          patternData = merge.recursive(true, patternData, context.hash);
        }

        // Add children e.g. {{#pattern "button"}}Click me{{/pattern}}
        if (context.fn) {
          patternData.children = context.fn(this);
        }

        const html = ghbs.handlebars.compile(
          fs.readFileSync(pattern.templateFilePath, 'utf8'),
        )(patternData, context);

        return new ghbs.handlebars.SafeString(html);
      } catch (e) {
        throw new Error(`Parse Error in Pattern ${name}: ${e.message}`);
      }
    }

    throw new Error(`Component \`${name}\` with template file \`${templateFile}.\` not found in folder \`${folder}\`.`);
  } catch (e) {
    return new ghbs.handlebars.SafeString(
      `<p>${e.message}</p>`,
    );
  }
}

// since the handlebars version used by gulp-handlebars is different
// than the version used withing this component helper here, we need register the
// component helper here as well, otherwise we couldn't have recursive
// components.
ghbs.handlebars.registerHelper('component', component);

export default component;
