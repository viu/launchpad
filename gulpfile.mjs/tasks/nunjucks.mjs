import browserSync from 'browser-sync';
import data from 'gulp-data';
import gulp from 'gulp';
import gulpif from 'gulp-if';
import gulpcached from 'gulp-cached';
import rename from 'gulp-rename';
import htmlmin from 'gulp-htmlmin';
import path from 'path';
import render from 'gulp-nunjucks-render';
import fs from 'fs';
import merge from 'merge';
import minimist from 'minimist';
import componentTag from '../lib/nunjucksExtensionComponent.mjs';
import removeBOM from '../lib/removeBOM.mjs';
import handleErrors from '../lib/handleErrors.mjs';
import config from '../lib/configReader.mjs';
import loadFilters from '../lib/loadFilters.mjs';

const argv = minimist(process.argv.slice(2));

// get data for templates
const getData = function getTemplateData(file) {
  let globalData = {};
  let packageData = {};

  // check for global.json
  const globalDataPath = path.resolve(config.root.src, config.tasks.data.src, 'global.json');
  if (fs.existsSync(globalDataPath)) {
    try {
      globalData = JSON.parse(removeBOM(fs.readFileSync(globalDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }
  // add package json into global data
  const packageDataPath = path.resolve(config.root.base, 'package.json');
  if (fs.existsSync(packageDataPath)) {
    try {
      packageData = JSON.parse(removeBOM(fs.readFileSync(packageDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }
  packageData = { package: packageData };
  globalData = merge.recursive(true, packageData, globalData);

  // add arguments to global data
  globalData.args = argv;

  // find relative data file path
  let pageDataPath = path.resolve(config.root.src, config.tasks.data.src, file.relative);
  // replace extension with json
  pageDataPath = `${pageDataPath.substring(0, pageDataPath.lastIndexOf('.'))}.json`;

  // replace duplicated folder separator
  pageDataPath = pageDataPath.replace(path.sep + path.sep, path.sep);

  // remove folders specified in config (so we don't need to put data files into /data/templates
  config.tasks.data.removeFromPath.forEach((pathToRemove) => {
    // eslint-disable-next-line no-param-reassign
    pathToRemove = pathToRemove.replace('/', path.sep);
    pageDataPath = pageDataPath.replace(pathToRemove, '');
  });

  // check if file exists - otherwise provide empty data
  let pageData = {};
  if (fs.existsSync(pageDataPath) && !fs.lstatSync(pageDataPath).isDirectory()) {
    try {
      pageData = JSON.parse(removeBOM(fs.readFileSync(pageDataPath, 'utf8')));
    } catch (e) {
      handleErrors(e);
    }
  }

  return merge.recursive(true, globalData, pageData);
};

// main task
function nunjucks(callback) {
  if (!config.tasks.nunjucks) {
    return callback();
  }

  // this solution appears to only be working if more than one entry is placed in excludeFolders
  const exclude = path.normalize(`!/**/{${config.tasks.nunjucks.excludeFolders.join(',')}}/**/*`);
  const srcPaths = [config.tasks.nunjucks.src, exclude];
  const filters = loadFilters(config.tasks.nunjucks.filters, config.root.base);

  return gulp
    .src(srcPaths, { cwd: config.root.src })
    .pipe(data(getData))
    .on('error', handleErrors)
    .pipe(gulpif(!global.production && config.tasks.nunjucks.cache, gulpcached('nunjucks-html')))
    .pipe(render({
      // Add node modules to path of gulp-nunjucks in order to be able to use templates
      // from node modules. Example: VIU Styleguide
      path: [path.posix.join(config.root.src, config.tasks.nunjucks.root), path.resolve(config.root.src, '..', 'node_modules')],
      envOptions: {
        watch: false,
      },
      manageEnv: (env) => {
        env.addExtension('component', componentTag);

        if (filters) {
          filters.forEach((filter) => {
            if (filter && filter.name && filter.filter) {
              env.addFilter(filter.name, filter.filter);
            }
          });
        }
      },
    }))
    .on('error', handleErrors)
    .pipe(gulpif(global.production, htmlmin(config.tasks.nunjucks.htmlmin)))
    .pipe(rename((template) => {
      // Updates the object in-place

      // eslint-disable-next-line no-param-reassign
      template.extname = `.${config.tasks.nunjucks.destExtension || 'html'}`;
    }))
    .pipe(gulp.dest(path.posix.join(config.root.dest, config.tasks.nunjucks.dest)))
    .on('end', browserSync.reload);
}

export default nunjucks;
export { loadFilters };
