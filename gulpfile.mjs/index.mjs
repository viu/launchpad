// gulpfile.mjs
// ===========
// Rather than manage one giant configuration file responsible
// for creating multiple tasks, each task has been broken out into
// its own file in gulpfile.mjs/tasks. Any files in that directory get
// automatically required below.

// To add a new task, add a new task file into that directory.
// gulpfile.mjs/tasks/default.mjs specifies the default set of tasks to run
// gulpfile.mjs/tasks/default.mjs specifies the default set of tasks to run
// when you run `gulp`.

import path from 'path';
import log from 'fancy-log';
import colors from 'ansi-colors';
import normalisePath from './lib/normalisePath.mjs';
import development from './tasks/development.mjs';
import debug from './tasks/debug.mjs';
import lint from './tasks/lint.mjs';
import production from './tasks/production.mjs';
import server from './tasks/server.mjs';

let lp;

try {
  log('Loading Launchpad');

  const lpPath = path.resolve(process.env.INIT_CWD, 'node_modules/@viu/launchpad/package.json');
  log('Loading Launchpad from', colors.magenta(lpPath));
  lp = await import(normalisePath(lpPath));
} catch (Ex) {
  lp = {
    name: 'VIU Launchpad',
    version: '?',
  };
}

// print VIU logo and launchpad version and project version to console
log(colors.red(`Starting ${lp.name}@${lp.version}

                  viiv
                 UUUUUU
                  vuuv

viuuuu     iiiiiiiUUUUi  uuuuu     uuuuu
 UUUUUi   iUUUUI iUUUUi  UUUUU     UUUUU
 iUUUUI   IUUUU  iUUUUi  UUUUU     UUUUU
  vUUUUv iUUUUi  iUUUUi  UUUUU     UUUUU
   UUUUU uUUUv   iUUUUi  UUUUU     UUUUU
   iUUUUUUUUU    iUUUUi  UUUUU    iUUUUU
    iUUUUUUUi    iUUUUi  iUUUUUUUUUUUUUU
     iVVVVVi     vUUUUv   vVUUUUVv  VIUU

`));

export default development;
export {
  development,
  debug,
  lint,
  production,
  server,
};
