import log from 'fancy-log';
import colors from 'ansi-colors';
import PluginError from 'plugin-error';
import prettifyTime from './prettifyTime.mjs';
import handleErrors from './handleErrors.mjs';
import config from './configReader.mjs';

const compileLogger = (err, stats) => {
  if (err) throw new PluginError('webpack', err);

  const compileTime = prettifyTime(stats.endTime - stats.startTime);
  const outputJson = stats.toJson('detailed');
  let statColor = stats.compilation.warnings.length < 1 ? 'green' : 'yellow';

  if (stats.compilation.errors.length > 0) {
    stats.compilation.errors.forEach((error) => {
      handleErrors(error);
      statColor = 'red';
    });
  } else {
    if (config.tasks.js.verbose) {
      log('\r\n', colors[statColor](stats), '\r\n');

      outputJson.modules.forEach((module) => {
        // TODO simplify if?
        if (
          (module.optimizationBailout instanceof Array && module.optimizationBailout.length > 0)
          || (module.usedExports instanceof Array && module.usedExports.length > 0)
          || (module.providedExports instanceof Array && module.providedExports.length > 0)) {
          // Only if we have info to provide, we log the item
          log('Item\t', colors.cyan(`#${module.id}\t`), colors.magenta(module.name));

          if (module.optimizationBailout instanceof Array) {
            module.optimizationBailout.forEach((bailout) => {
              log(colors.yellow(`${bailout}`));
            });
          }

          // TODO check how to best log tree shaking...
          if (module.usedExports instanceof Array) {
            let exports = '';
            module.usedExports.forEach((usedExport) => {
              exports += `${usedExport} `;
            });
            log('Used:\t', colors.green(exports));
          }

          // TODO check how to best log tree shaking...
          if (module.providedExports instanceof Array) {
            let exports = '';
            module.providedExports.forEach((providedExport) => {
              exports += `${providedExport} `;
            });
            log('Provided:\t', colors.green(exports));
          }
        }
      });
    }
    log('Compiled with', colors.cyan('webpack'), 'in', colors.magenta(compileTime));
  }
};

export default compileLogger;
