import { createRequire } from 'module';
import log from 'fancy-log';
import path from 'path';

const require = createRequire(import.meta.url);

const loadFilters = (filterPaths, basePath = '') => {
  const filters = [];

  if (filterPaths) {
    filterPaths.forEach((filterPath) => {
      let fullPath;

      try {
        fullPath = path.resolve(
          process.cwd(),
          basePath,
          filterPath,
        );

        // eslint-disable-next-line global-require
        const filter = require(fullPath);

        if (filter && filter.name && filter.filter) {
          filters.push(filter);
        }
      } catch (errNotCalledFromGulpAndInitCWDUndefined) {
        log('Could not load filter from path:', fullPath, errNotCalledFromGulpAndInitCWDUndefined);
      }
    });
  }

  return filters;
};

export default loadFilters;
