import normalisePath from './normalisePath.mjs';

const pathToFileURL = (filePath) => new URL(`file://${normalisePath(filePath)}`).href;

export default pathToFileURL;
