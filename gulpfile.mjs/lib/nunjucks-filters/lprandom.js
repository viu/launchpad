module.exports = {
  name: 'lprandom',
  filter(number) {
    // Returns a random integer between 1(included) and number (excluded)
    // Using Math.round() will give you a non-uniform distribution!
    return Math.floor(Math.random() * (number - 1)) + 1;
  },
};
