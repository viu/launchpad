import path from 'path';
import { promises as fs } from 'fs';
import merge from 'merge';
import log from 'fancy-log';
import colors from 'ansi-colors';

async function loadPackageJson() {
  const configPath = path.resolve('./config.json');
  const pkgPath = path.resolve(process.env.INIT_CWD, 'package.json');
  let config = {};

  // load base configuration
  try {
    const configContent = await fs.readFile(configPath);
    const parsedConfigContent = JSON.parse(configContent);
    config = parsedConfigContent;
  } catch (error) {
    log(colors.red(`Failed to load base configuration from ${configPath}: ${error.message}`));
  }

  // update based config with project config if available
  try {
    const pkgContent = await fs.readFile(pkgPath);
    const parsedPkgContent = JSON.parse(pkgContent);
    config = merge.recursive(true, config, parsedPkgContent.config);
    log('Loaded configuration from', colors.magenta(pkgPath));
  } catch (error) {
    log(colors.red(`Failed to load configuration from ${pkgPath}: ${error.message}`));
  }

  return config;
}

const config = await loadPackageJson();
export default config;
