import path from 'path';
import webpack from 'webpack';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';
import config from './configReader.mjs';
import normalisePath from './normalisePath.mjs';
import WebpackManifestPlugin from './webpackManifest.mjs';

function createWebpackConfig(env) {
  if (!config.tasks.js) return {};

  const jsSrc = path.resolve(config.root.src, config.tasks.js.src);
  const nodeModuleSrc = 'node_modules';
  const jsDest = path.resolve(config.root.dest, config.tasks.js.dest);
  const publicPath = normalisePath(config.tasks.js.dest, '/');
  const extensions = config.tasks.js.extensions.map((extension) => `.${extension}`);

  const rev = config.tasks.production.rev && env === 'production';
  const filenamePattern = rev ? '[name]-[fullhash].js' : '[name].js';
  const chunkFileNamePattern = rev ? '[name]-chunk-[chunkhash].js' : '[name]-chunk.js';

  let plugins = config.tasks.js.plugins.map((plugin) => {
    const pluginConfig = {};
    pluginConfig[plugin.reference] = plugin.name;
    return new webpack.ProvidePlugin(pluginConfig);
  });

  // `config.tasks.js.targetBuildVariables`
  // Usage:
  // 1. List the build-target-specific variables you like to use in your scripts,
  // in tasks.js.targetBuildVariables
  // 2.A Define new npm scripts for all the target-environments you like to have,
  // Environment-Variables to a specific value.
  // or:
  // 2.B Set the Environment-Variables (E.g. 'API_USERS') directli via CLI
  // (plattform-specific command)
  // 3. Use the variables in your javascript files.
  // Note that 'API_USERS' will be available as __API_USERS__ in your Script.
  //
  //
  // Basic scenario would be that you want to have different build, which test
  // different API's, for instance:
  // Test-API on: "test.example.com"
  // Develop-API on: "dev.example.com"
  //
  // Example package.JSON: (Only with specific 'develop' script. You probably
  // have to define the other Scripts like
  // "production_test", "production_develop" etc. as well.)
  // `
  //     "tasks": {
  //       "js": {
  //         "targetBuildVariables": [
  //           "API_USERS",
  //           "API_PRODUCTS"
  //         ]
  //       }
  //     },
  //     "scripts": {
  //        "develop": "gulp --gulpfile node_modules/@viu/launchpad/gulpfile.mjs/index.mjs",
  //        "develop_test": cross-env API_USERS=test.example.com/getusers
  // API_PRODUCTS=test.example.com/getproducts npm run develop",
  //        "develop_dev": "cross-env API_USERS=dev.example.com/getusers
  // API_PRODUCTS=dev.example.com/getproducts npm run develop"
  //     }
  // `
  //
  // IMPORTANT: Note the `cross-env` call at the beginning of the npm-scripts
  // `develop_test` and `develop_dev`. The `cross-env` package will make sure
  // that setting the environment-variable (e.g. `API_USERS`) will work
  // cross-platform.
  //
  // With this configuration, you can use your custom variables in your javascript
  // files like following:
  //
  // `
  // console.log(__API_USERS__);
  // console.log(__API_PRODUCTS__);
  // `
  //
  // `npm run develop_test`
  // => 'test.example.com/getusers'
  // => 'test.example.com/getproducts'
  //
  // `npm run develop_dev`
  // => 'dev.example.com/getusers'
  // => 'dev.example.com/getproducts'
  //
  // Note: If you have set the environment variable directly via CLI,
  // you would run 'npn run develop (or whatever)'.
  //
  plugins = [
    ...plugins,
    ...config.tasks.js.targetBuildVariables.map((targetBuildVariable) => (
      new webpack.DefinePlugin({
        [`__${targetBuildVariable}__`]: JSON.stringify(process.env[targetBuildVariable]),
      })
    )),
  ];

  // BundleAnalyzerPlugin
  // Don't do this while testing!
  const ba = config.tasks.js.bundleAnalyzerPlugin || { enabled: false };

  if (ba.enabled && env !== 'test') {
    const port = ba.port || 0;
    let mode = 'disabled';
    if (env === 'production') {
      mode = ba.productionMode || mode;
    } else if (env === 'debug') {
      mode = ba.debugMode || mode;
    } else {
      mode = ba.developMode || mode;
    }

    plugins.push(new BundleAnalyzerPlugin({
      analyzerMode: mode,
      analyzerPort: port,
      analyzerHost: ba.analyzerHost,
      reportFilename: ba.reportFilename,
      defaultSizes: ba.defaultSizes,
      openAnalyzer: ba.openAnalyzer,
      generateStatsFile: ba.generateStatsFile,
      statsFilename: ba.statsFilename,
      statsOptions: ba.statsOptions,
      logLevel: ba.logLevel,
    }));
  }

  config.tasks.js.contextReplacementPlugins.map(
    (plugin) => plugins.push(new webpack.ContextReplacementPlugin(
      new RegExp(plugin.context),
      new RegExp(plugin.replacement),
    )),
  );

  // Only do module concatenation on non-watch tasks
  // See: https://github.com/webpack/webpack/issues/5132
  // TODO remove checks, once issue is resolved?
  // Possibly it's anyhow not desired for develop task.
  if ((env === 'debug' || env === 'production') && config.tasks.js.moduleConcatenation) {
    plugins.push(new webpack.optimize.ModuleConcatenationPlugin());
  }

  const loaders = [
  ];

  if (config.tasks.js.loaders) {
    for (let i = 0; i < config.tasks.js.loaders.length; i += 1) {
      const loader = config.tasks.js.loaders[i];

      loader.test = new RegExp(loader.test, 'i');
      loader.exclude = new RegExp(loader.exclude, 'i');
      loaders.push(loader);
    }
  }

  const webpackConfig = {
    mode: 'production',
    context: jsSrc,
    plugins,
    resolve: {
      modules: [
        jsSrc,
        nodeModuleSrc,
      ],
      extensions,
    },
    module: {
      rules: loaders,
    },
  };

  if (env === 'development') {
    webpackConfig.mode = 'development';
    webpackConfig.devtool = 'inline-source-map';

    // Create new entries object with webpack-hot-middleware added
    Object.keys(config.tasks.js.entries).forEach((key) => {
      const entry = config.tasks.js.entries[key];
      config.tasks.js.entries[key] = ['webpack-hot-middleware/client?&reload=true'].concat(entry);
    });

    webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());

    webpackConfig.optimization = {
      minimize: false,
    };
  }

  if (env !== 'test') {
    // Karma doesn't need entry points or output settings
    webpackConfig.entry = config.tasks.js.entries;

    webpackConfig.output = {
      path: path.normalize(jsDest),
      filename: filenamePattern,
      chunkFilename: chunkFileNamePattern,
      publicPath,
    };
  }

  if (env === 'production') {
    if (rev) {
      webpackConfig.plugins.push(new WebpackManifestPlugin(publicPath, config.root.dest));
    }
    webpackConfig.plugins.push(
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
        },
      }),
      new webpack.NoEmitOnErrorsPlugin(),
    );
  }

  if (env === 'debug') {
    webpackConfig.devtool = 'source-map';
    if (rev) {
      webpackConfig.plugins.push(new WebpackManifestPlugin(publicPath, config.root.dest));
    }

    // as of webapck 4 the default for mode = 'production' is minimization
    // out of the box for debug we want this disabled
    webpackConfig.optimization = {
      minimize: false,
    };

    webpackConfig.plugins.push(
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
        },
      }),
      new webpack.NoEmitOnErrorsPlugin(),
    );
  }

  // Set additional configurations in webpackConfig
  // Attention: it's possible to overwrite all previously set properties
  // in webpackConfig with this setting
  Object.keys(config.tasks.js.additionalWebpackSettings).forEach((key) => {
    webpackConfig[key] = config.tasks.js.additionalWebpackSettings[key];
  });

  return webpackConfig;
}

export default createWebpackConfig;
