import path from 'path';

// Normalizes Windows file paths to valid url paths
const normalisePath = (...args) => path.posix.join(...args).replace(/\\/g, '/');

export default normalisePath;
