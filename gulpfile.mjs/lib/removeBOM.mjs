const removeBOM = (fileContents) => fileContents.replace(/^\ufeff/g, '');

export default removeBOM;
