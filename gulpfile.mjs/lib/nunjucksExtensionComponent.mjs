import render from 'gulp-nunjucks-render';
import path from 'path';
import fs from 'fs';
import merge from 'merge';
import removeBOM from './removeBOM.mjs';
import handleErrors from './handleErrors.mjs';
import config from './configReader.mjs';
import loadFilters from './loadFilters.mjs';

const fileExists = (filePaths) => {
  for (let i = 0; i < filePaths.length; i += 1) {
    if (fs.existsSync(filePaths[i])) {
      return filePaths[i];
    }
  }

  return null;
};

/**
 * {% component 'name', {title: 'Example', subtitle: 'An example component'} %}
 */
const ComponentTag = function componentTag() {
  this.tags = ['component'];

  // eslint-disable-next-line no-unused-vars
  this.parse = (parser, nodes, lexer) => {
    const token = parser.nextToken();

    const args = parser.parseSignature(null, true);
    parser.advanceAfterBlockEnd(token.value);

    return new nodes.CallExtension(this, 'run', args);
  };

  this.run = (context, name, data) => {
    let componentPath = '';
    let componentName = name;
    let dataPath = '';
    let filePath = '';
    const filters = loadFilters(config.tasks.nunjucks.filters, config.root.base);

    // components are organized in this manner: components/componentName/componentName.html
    // Note that the component is only given the data object, and the context is not exposed.
    // data ist merged from data files (components/componentName/componentName.json)
    // and the data provided in the component tag
    // data provided in the component tag overrides default content from component
    // check if file exists - otherwise provide empty data

    // for components coming from defined node-modules
    // this functionality may not actually be used and might be dropped for 3.0.0
    if (componentName.indexOf('::') === 0) {
      componentName = componentName.replace('::', '');
      dataPath = path.resolve(config.root.src, '..', 'node_modules', componentName, 'index.json');
      filePath = path.resolve(config.root.src, '..', 'node_modules', componentName, 'index.hbs');
    } else {
      const folders = config.tasks.nunjucks.componentFolders;
      const extensions = config.tasks.nunjucks.componentExtensions;
      for (let i = 0; i < folders.length; i += 1) {
        componentPath = path.resolve(config.root.src, folders[i], componentName);
        dataPath = path.resolve(componentPath, `${componentName}.json`);
        filePath = fileExists(
          extensions.map(
            // eslint-disable-next-line no-loop-func
            (extension) => path.resolve(componentPath, `${componentName}.${extension}`),
          ),
        );
        // once a file is found, we exit the search loop right away
        if (fs.existsSync(dataPath) || fs.existsSync(filePath)) {
          break;
        }
      }
    }

    if (fs.existsSync(dataPath)) {
      let defaultData = {};
      try {
        defaultData = JSON.parse(removeBOM(fs.readFileSync(dataPath, 'utf8')));
      } catch (e) {
        handleErrors(e);
      }

      // eslint-disable-next-line no-param-reassign
      data = merge(true, defaultData, data);
      // could be replaced with the following line to merge recursively
      // not sure what makes more sense
      // data = merge.recursive(true, defaultData, data);
    }

    const env = render.nunjucks.configure(
      // Add node modules to path of gulp-nunjucks in order to be able to use templates
      // from node modules. Example: Usage of npm packages for components
      [path.posix.join(config.root.src, config.tasks.nunjucks.root), path.resolve(config.root.src, '..', 'node_modules')],
      { autoescaping: false, watch: false },
    );
    env.addExtension('component', new ComponentTag());
    if (filters) {
      filters.forEach((filter) => {
        if (filter && filter.name && filter.filter) {
          env.addFilter(filter.name, filter.filter);
        }
      });
    }

    return context.env.filters.safe(render.nunjucks.render(filePath, data));
  };
};

export default new ComponentTag();
