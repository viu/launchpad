import staticFiles from '../tasks/static.mjs';
import fonts from '../tasks/fonts.mjs';
import images from '../tasks/images.mjs';
import videos from '../tasks/videos.mjs';
import svgSprite from '../tasks/svgSprite.mjs';
import nunjucks from '../tasks/nunjucks.mjs';
import handlebars from '../tasks/handlebars.mjs';
import css from '../tasks/css.mjs';
import webpackDebug from '../tasks/webpackDebug.mjs';
import webpackProduction from '../tasks/webpackProduction.mjs';

const getEnabledTasks = (env) => {
  const assetTasks = [fonts, images, svgSprite, videos];
  const codeTasks = [handlebars, nunjucks, css];
  const watchableTasks = [fonts, images, svgSprite, nunjucks, handlebars, css, staticFiles];

  if (env === 'production') {
    codeTasks.push(webpackProduction);
  } else if (env === 'debug') {
    codeTasks.push(webpackDebug);
  }

  return {
    assetTasks,
    codeTasks,
    watchableTasks,
  };
};

export default getEnabledTasks;
