import path from 'path';
import fs from 'fs';

class WebpackManifestPlugin {
  constructor(publicPath, dest, filename) {
    this.filename = filename || 'rev-manifest.json';
    this.publicPath = publicPath;
    this.dest = dest;
  }

  apply(compiler) {
    const { filename } = this;
    const { publicPath } = this;
    const { dest } = this;

    compiler.hooks.done.tap('WebpackManifestPlugin', (stats) => {
      const chunks = stats.toJson().assetsByChunkName;
      const manifest = {};

      Object.keys(chunks).forEach((key) => {
        const originalFilename = `${key}.js`;
        chunks[key].forEach((chunk) => {
          manifest[path.posix.join(publicPath, originalFilename)] = path.posix.join(
            publicPath,
            chunk,
          );
        });
      });

      fs.writeFileSync(
        path.posix.join(process.cwd(), dest, filename),
        JSON.stringify(manifest),
      );
    });
  }
}

export default WebpackManifestPlugin;
