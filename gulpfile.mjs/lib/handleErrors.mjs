import log from 'fancy-log';

const handleErrors = (errorObject, callback) => {
  let errStr = errorObject.toString();
  errStr = errStr.split(': ').join(':\n');

  if (errStr.indexOf('[object Object]') === 0) {
    if (typeof errorObject.message === 'string') {
      errStr = errorObject.message;
    } else {
      try {
        errStr = JSON.stringify(errorObject);
      } catch (e) {
        errStr = '[object Object] // Could not parse the error Message. You might need to debug with a console.log statement in launchpad.';
      }
    }
  }

  log(errStr);

  // // Keep gulp from hanging on this task
  if (typeof callback !== 'undefined' && typeof callback.emit === 'function') callback.emit('end');
};

export default handleErrors;
